#Include "Protheus.ch"
#Include "RESTFUL.ch"
#Include "tbiconn.ch"
#Include "aarray.ch"
#Include "json.ch"

/*
	{Protheus.doc}  Consulta Cliente     
	WebService REST                      
	@author  	Milton J.dos Santos      
	@since   	01/01/2023               
	@version	P12                      
	@history                             
*/
#Define Desc_Rest 	"Serviço REST para disponibilizar a Consulta Cliente"
#Define Desc_Get  	"Retorna a lista de Clientes" 

User Function GetCli()

Return

WSRESTFUL rGetCli DESCRIPTION Desc_Rest

WSDATA CNPJ 	As String


WSMETHOD GET  DESCRIPTION Desc_Get  WSSYNTAX "/rGetCli || /rGetCli/{}"

END WSRESTFUL

/*
***********************************************************************************************
* {Protheus.doc}  GET                                                                         *
* @author Milton J.dos Santos                                                                 *  
* Processa as informações e retorna o json                                                    *
* @since 	17/06/2019                                                                        *
* @version undefined                                                                          *
* @param oSelf, object, Objeto contendo dados da requisição efetuada pelo cliente, tais como: *
*    - Parâmetros querystring (parâmetros informado via URL)                                  *
*    - Objeto JSON caso o requisição seja efetuada via Request Post                           *
*    - Header da requisição                                                                   *
*    - entre outras ...                                                                       *
* @type Method                                                                                *
***********************************************************************************************
*/

WSMETHOD GET WSRECEIVE CNPJ WSSERVICE rGetCli

Local cJsonRet
Local cAgora
Local lRet 		:= .F.		
Local cCNPJ		:= ""
Local cDirSQL	:= "\Query"
Local cArqSQL 	:= "" 		
Local uAlias	:= GetNextAlias()
Local bWhile	:= {|| .NOT. (  (uAlias)->(eof()))}
Local bSkip		:= {|| (uAlias)->(dbSkip())}
Local ENTER		:= Chr(13) + Chr(10)

	cJsonRet 	:= ""	
	
	cCNPJ		:= Self:CNPJ

	If cCNPJ == 'ALL'
		cCNPJ := " "
	Endif

	If cCNPJ == NIL .or. Empty( cCNPJ )
		cMsg	:= "Conteudo invalido !"
	ElseIf Valtype( cCNPJ ) <> "C"
		cMsg	:= "Erro no formato do parametro mes e ano recebido --> " +  Valtype( cCNPJ )
	ElseIf Empty( cCNPJ )
		cMsg	:= "Faltou informar o C.N.P.J. !"
	Else
		lRet	:= .T.

		cAgora 	:= "_D_" + AllTrim( DtOS( Date() ) ) 
		cAgora	+= "_H_" + SubStr(Time(),1,2 )
		cAgora	+= "_M_" + SubStr(Time(),4,2 )

		ConOut("Requisicao de Consulta Cliente : " + cAgora )
	
		// Pesquisa CADASTRO DE CLIENTES
		cQuery := " SELECT	*" + ENTER
		cQuery += " FROM " + RetSqlNAME("SA1") + " SA1 " 			+ ENTER
		cQuery += " WHERE	SA1.D_E_L_E_T_ <> '*'" 					+ ENTER
		If ! Empty( cCNPJ )
			cQuery += "		AND A1_CGC = '" + cCNPJ + "'" 			+ ENTER
		ENDIF
//		cQuery += "			AND A1_MSBLQL			<> '1' "		+ ENTER
		cQuery += " ORDER BY	A1_COD" 							+ ENTER
			
		If .not. Empty( cDirSQL )

			// Cria o diretório para salvar os arquivos de SQL
			If !ExistDir( cDirSQL )
				MakeDir( cDirSQL )
			EndIf
			cArqSQL 	:= "GetCli"	+ cAgora + ".SQL"
			MemoWrite( cDirSQL + '/' + cArqSQL , cQuery)

		Endif
		MPSysOpenQuery( cQuery, uAlias )
	
		dbSelectArea(uAlias)
	
		IF ( eval(bWhile))
			cJsonRet := ''
			nX	:= 1
			cJsonRet  := '{ "CLIENTE":[ ' 
			While ( eval(bWhile) )
				If nX > 1
					cJsonRet  +=' , '
				EndIf				
				cJsonRet  += '{'
				cJsonRet  += '  "A1_COD":"'			+ AllTrim( (uAlias)->A1_COD		)
				cJsonRet  += '  ","A1_LOJA":"'		+ AllTrim( (uAlias)->A1_LOJA	)
				cJsonRet  += '  ","A1_PESSOA":"'	+ AllTrim( (uAlias)->A1_PESSOA	)
				cJsonRet  += '  ","A1_CGC":"'		+ AllTrim( (uAlias)->A1_CGC		)
				cJsonRet  += '  ","A1_NOME":"'		+ AllTrim( (uAlias)->A1_NOME	)
				cJsonRet  += '  ","A1_NREDUZ":"'	+ AllTrim( (uAlias)->A1_NREDUZ	)
				cJsonRet  += '  ","A1_END":"'		+ AllTrim( (uAlias)->A1_END		)
				cJsonRet  += '  ","A1_TIPO":"'		+ AllTrim( (uAlias)->A1_TIPO	)
				cJsonRet  += '  ","A1_EST":"'		+ AllTrim( (uAlias)->A1_EST		)
				cJsonRet  += '  ","A1_ESTADO":"'	+ AllTrim( (uAlias)->A1_ESTADO	)
				cJsonRet  += '  ","A1_COD_MUN":"'	+ AllTrim( (uAlias)->A1_COD_MUN	)
				cJsonRet  += '  ","A1_MUN":"'		+ AllTrim( (uAlias)->A1_MUN		)
				cJsonRet  += '  ","A1_BAIRRO":"'	+ AllTrim( (uAlias)->A1_BAIRRO	)
				cJsonRet  += '  ","A1_NATUREZ":"'	+ AllTrim( (uAlias)->A1_NATUREZ	)
				cJsonRet  += '  ","A1_CEP":"'		+ AllTrim( (uAlias)->A1_CEP		)
				cJsonRet  += '  ","A1_DDD":"'		+ AllTrim( (uAlias)->A1_DDD		)
				cJsonRet  += '  ","A1_TEL":"'		+ AllTrim( (uAlias)->A1_TEL		)
				cJsonRet  += '  ","A1_PAIS":"'		+ AllTrim( (uAlias)->A1_PAIS	)
				cJsonRet  += '  ","A1_CONTATO":"'	+ AllTrim( (uAlias)->A1_CONTATO	)
				cJsonRet  += '  ","A1_INSCR":"'		+ AllTrim( (uAlias)->A1_INSCR	)
				cJsonRet  += '  ","A1_VEND":"'		+ AllTrim( (uAlias)->A1_VEND	)
				cJsonRet  += '  ","A1_REGIAO":"'	+ AllTrim( (uAlias)->A1_REGIAO	)
				cJsonRet  += '  ","A1_CONTA":"'		+ AllTrim( (uAlias)->A1_CONTA	)
				cJsonRet  += '  ","A1_COND":"'		+ AllTrim( (uAlias)->A1_COND	)
				cJsonRet  += '  ","A1_RISCO":"'		+ AllTrim( (uAlias)->A1_RISCO	)
				cJsonRet  += '  ","A1_MOEDALC":"'	+ AllTrim( (uAlias)->A1_MOEDALC	)
				cJsonRet  += '  ","A1_LC":"'		+ AllTrim( (uAlias)->A1_LC		)
				cJsonRet  += '  ","A1_VENCLC":"'	+ AllTrim( (uAlias)->A1_VENCLC	)
				cJsonRet  += '  ","A1_EMAIL":"'		+ AllTrim( (uAlias)->A1_EMAIL	)
				cJsonRet  += '  ","A1_CNAE":"'		+ AllTrim( (uAlias)->A1_CNAE	)
				cJsonRet  += '  ","A1_MSBLQL":"'	+ AllTrim( (uAlias)->A1_MSBLQL	)
				cJsonRet  += '  ","A1_CODPAIS":"'	+ AllTrim( (uAlias)->A1_CODPAIS	)
				cJsonRet  += '"}'
				eval(bSkip)
				nX:= nX+1
			Enddo
			cJsonRet  += ']'	
			cJsonRet  += '}'
		Else
			cMsg	:= OEMtoANSI("C.N.P.J. inexistente no cadastro : ") + cCNPJ + " "
			cJsonRet:= '{"msg":"Erro : ' + cMsg + '"}'
		Endif
		//Fecha a tabela
		(uAlias)->(dbCloseArea())

		::SetResponse( cJsonRet )

	Endif
	If .not. lRet

		ConOut( cMsg  )
		SetRestFault(400, cMsg )

	Endif

Return .T.
