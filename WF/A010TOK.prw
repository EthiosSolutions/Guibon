#include "TOTVS.ch"                                                  
#INCLUDE "colors.ch"
#include "topconn.ch"                
#include "rwmake.ch"        
#include "tcbrowse.ch"

/*/{Protheus.doc} A010TOK

    Workflow de compras

    @author     Milton
	@since		01/01/2023
    @version    1.0
/*/

User Function A010TOK()

Local cMsg		:= "" 
Local _CRLF 	:= chr(13)+chr(10)
Local _cEmail   := GetMv("HP_MAILPRO")
Local cBloq		:= IIF(M->B1_MSBLQL=="1","Sim","N?o")
Local cAssunto	:= ""  
Local lRet		:= .T.
Local cCodServ  := M->B1_CODISS
Local cTipo     := M->B1_TIPO
Local cCodBar   := M->B1_CODBAR
Local cCodima   := M->B5_ECIMGFI
Local cMsg		:= "http://gtin.hopelingerie.com.br"							

IF cTipo == "SV" .AND. !(__cuserid $ GETMV("HP_INCPROD"))
	MsgAlert("Cadastro de produtos do tipo servi?o, solicitar ao setor fiscal! ","Aten��o")
	lRet := .F.
ENDIF

IF cTipo == "SV" .AND. Empty(cCodServ)
	MsgAlert("Favor, preencher o codigo de servi?o","Aten��o")
	lRet := .F.
ENDIF

IF lRet 

	IF Inclui 
		cAssunto	:="Inclus?o cadastro de produto"
	Else 
		cAssunto	:="Altera??o cadastro de produto"
	ENDIF		

	cMsg += '<html>'   //Monta corpo do e-mail em HTML
	cMsg += '<head>'
	cMsg += '<title></title>'
	cMsg += '</head>'
	cMsg += '<BODY>'
	cMsg += '<font size="3" face="Arial"><b><br>'+cAssunto+'</b></br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Codigo 	  : '+alltrim(M->B1_COD)+'</br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Descri??o    : '+alltrim(M->B1_DESC)+'</br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Bloqueado    : '+cBloq+'</br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Usu?rio      : '+USRRETNAME(RETCODUSR())+'</br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Data         : '+dtoc(date())+'</br></font>' +_CRLF
	cMsg += '<font size="2" face="Arial"><br>Hora 	  	  : '+Time()+'</br></font>' +_CRLF
	cMsg += '<font size="1" face="Arial"><br>Obs : E-mail enviado automaticamente pelo sistema.<br/></font>' +_CRLF
	cMsg += '</BODY>'
	cMsg += '</html>'

	U_SEndMail(_cEmail,cAssunto,cMsg,"")               '

ENDIF
Return lRet
