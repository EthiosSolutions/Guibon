#INCLUDE "TOTVS.CH"
#INCLUDE "topconn.ch"

/*/{Protheus.doc} MT121BRW
	Ponto de entrada para bot�o de reenvido do Workflow 
	@author Milton J.dos Santos
	@since	01/01/2023                                                   
/*/

User Function MT121BRW()

aAdd( aRotina, { "Reenviar WorkFlow", "U_WFREENV()"		, 5, 0, 5 } ) 
	
return

User Function WFREENV
Local cNum := SC7->C7_NUM

	DbSelectArea("SCR")		// Tabela de Alcada de Aprovacao
	SCR->(DbSetOrder(1))	// Indice CR_FILIAL+CR_TIPO+CR_NUM+CR_NIVEL
	If SCR->(DbSeek(xFilial("SCR")+"PC"+PadR(cNum,TamSx3("CR_NUM")[1])))
		If ( SCR->CR_STATUS $ "01/02")
			U_WFW120P()
			MSGINFO("WorkFlow Reeviado com sucesso")
		Else
			MsgSTOP("Nao foi possivel enviar WorkFlow. Nao tem pendencia de aprovacao!!!")
	    Endif 		
	Else
		MsgSTOP("Nao tem registro de aprovacao!!!")
	Endif 

Return


